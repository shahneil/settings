# Docker

## Installation

Retrieve and run Docker install script:
```
$ wget -qO- https://get.docker.com/ | sh
```
Add non-root user to local docker Unix group:
```
$ sudo usermod -aG docker [user]
```
Log out and back in for group membership to take effect.
To verify installation, run following commands:
```
$ docker --version
$ docker system info
```

Upgrading Docker CE:
```
$ sudo apt-get update
$ sudo apt-get remove docker docker-engine docker-ce docker.io -y
$ wget -qO- https://get.docker.com | sh
$ systemctl enable docker  # automatically start each time system boots
$ systemctl is-enabled docker  # check
$ docker container ls
$ docker service ls
```


## Useful commands

Images:
```
$ docker image pull [OPTIONS] NAME[:TAG|@DIGEST]  # Pull images
$ docker image ls [OPTIONS] [REPOSITORY[:TAG]]  # List images (to see SHA256 hashes add --digests flag)
$ docker image inspect [OPTIONS] IMAGE [IMAGE...]  # Image layer data and metadata
$ docker image rm [OPTIONS] IMAGE [IMAGE...]  # Delete images (cannot delete image associated with running/stopped containers)
```

Containers:
```
$ docker container ls
$ docker container run -it <image> <command>  # Create new container
$ docker container exec <options> <name/id> <command/app>  # Connect to running container
$ docker container stop <name or id>  # Stop container
$ docker container rm <name or id>  # Delete container
```














