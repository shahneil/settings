# Jenkins

Initial server setup:
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04

Oracle Java 8:
https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04

Jenkins:
https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-16-04

Docker:
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04

Nginx:
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04

Nginx Let's Encrypt:
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04

Nginx Jenkins SSL:
https://www.digitalocean.com/community/tutorials/how-to-configure-jenkins-with-ssl-using-an-nginx-reverse-proxy

Jenkins CI:
https://www.digitalocean.com/community/tutorials/how-to-set-up-continuous-integration-pipelines-in-jenkins-on-ubuntu-16-04
