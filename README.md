# Settings

## VSCode

Extensions:
- C/C++ (Microsoft)
- ESLint (Dirk Baeumer)
- Jinja (wholroyd)
- markdownlint (David Anson)
- Material Icon Theme (Philipp Kief)
- One Dark Pro (zhuangtongfa)
- Python (Microsoft)

User Settings:
- See settings.json

## React

Install eslint, babel-eslint, and eslint-plugin-react:
```
$ npm install -g eslint
$ npm install --save-dev babel-eslint eslint-plugin-react
```

Create .eslintrc file in root directory.
- See .eslintrc

## Ubuntu

### On clients:

__Generate RSA key:__  
Default location of RSA key is ```~/.ssh```
```
$ ssh-keygen -b 4096  # set passphrase for security
```

__Transfer public key to server and check that correct keys were added:__  
```
$ ssh-copy-id -i ~/.ssh/id_rsa.pub [user]@[host]
$ ssh [user]@[host]
Enter passphrase for key '~./ssh/id_rsa': [passphrase]
Welcome to Ubuntu 16.04.4 LTS...
[...]
(server) $ cat ~/.ssh/authorized_keys 
```

### On servers:

__Set requests to prefer IPv4:__  
Uncomment following line in ```/etc/gai.conf```
```
#precedence ::ffff:0:0/96  100
```

__Install/configure OpenSSH:__  
```
$ sudo apt-get install vim openssh-client openssh-server -y
```
Edit ```/etc/ssh/sshd_config```
```
AuthorizedKeysFile %h/.ssh/authorized_keys
PermitRootLogin no
PasswordAuthentication no

$ sudo service ssh restart
```

__Configure firewall:__  
```
sudo apt-get install ufw
sudo ufw allow ssh
sudo ufw allow http
sudo ufw enable
sudo ufw status verbose
```

__Secure shared memory:__    
Add the following line to ```/etc/fstab```
```
tmpfs /run/shm tmpfs defaults,noexec,nosuid 0 0
```
Reboot the server.

__Harden network:__  
Add the following to ```/etc/sysctl.conf```
```
# IP Spoofing protection
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1

# Ignore ICMP broadcast requests
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Disable source packet routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0 
net.ipv4.conf.default.accept_source_route = 0
net.ipv6.conf.default.accept_source_route = 0

# Ignore send redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

# Block SYN attacks
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_syn_backlog = 2048
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 5

# Log Martians
net.ipv4.conf.all.log_martians = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1

# Ignore ICMP redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0 
net.ipv6.conf.default.accept_redirects = 0

# Ignore Directed pings
net.ipv4.icmp_echo_ignore_all = 1
```
Reload sysctl with latest changes:
```
$ sudo sysctl -p
```

__Install git, fail2ban:__  
```
sudo apt-get install git fail2ban -y
```

__Fail2ban:__  
To configure, edit ```/etc/fail2ban/jail.local```
To restart:
```
$ sudo service fail2ban restart
```
To check the status:
```
$ sudo fail2ban-client status
```

__Oracle JDK 8:__  
```
$ sudo apt-add-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
$ sudo apt install oracle-java8-set-default
$ java -version  # check
```








